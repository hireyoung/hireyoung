class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.string :name
      t.string :desc
      t.string :main_skill

      t.timestamps
    end
  end
end
