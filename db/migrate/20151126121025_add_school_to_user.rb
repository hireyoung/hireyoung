class AddSchoolToUser < ActiveRecord::Migration
  def change
  	add_column :users, :university, :string
  	add_column :users, :faculty, :string
  	add_column :users, :specialization, :string
  end
end
