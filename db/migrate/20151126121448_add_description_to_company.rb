class AddDescriptionToCompany < ActiveRecord::Migration
  def change
  	add_column :companies, :website, :string
  	add_column :companies, :desc, :string
  end
end
