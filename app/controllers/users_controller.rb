class UsersController < ApplicationController
	before_action :signed_in_user, only: [:show,:edit, :update]
	before_action :correct_user,   only: [:show,:edit, :update]
  def create
    @user = User.new(user_params)  
    if @user.save
      sign_in @user
      flash[:success] = "Thank you for registration. We are glad having your here and work hard to help you with getting first job. No worries, you will get a text when everything is ready."
      redirect_to @user
    else
    	flash[:error] = "Try again! Failed fields are marked with x"
      render 'new'
    end
  end
  
  def new
    @user = User.new
    if signed_in? 
      redirect_to(current_user)
    end
  end

  def show
  	@user = User.find(params[:id])
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :country, :city)
    end
  def signed_in_user
      redirect_to signin_url, notice: "Please sign in." unless signed_in?
  end
  def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
end
