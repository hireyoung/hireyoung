class StaticPagesController < ApplicationController
  def index
	@visitor = Visitor.new()
  end
  def create
    @visitor = Visitor.new(visitor_params)  
    @visitor.save
    render :nothing => true
  end
  private
    def visitor_params
      params.require(:visitor).permit(:email)
    end
end
